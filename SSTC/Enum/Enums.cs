﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Enum
{
    public class Enums
    {
        public enum RegistryStatus : int
        {
            W_Akceptacji = 0,
            Zatwierdzony_Etap_1 = 2,
            Zatwierdzony_Etap_2 = 3,
            Odrzucony = 4

        }

        public enum UserRoles : int
        {
            Admin = 1,
            Manager = 2,
            User = 3,
            Accountant = 4
        }

        public enum MotionStatus : int
        {
            W_Akceptacji = 0,
            Zatwierdzony_Etap_1 = 2,
            Zatwierdzony = 3,
            Odrzucony = 4

        }
    }
}