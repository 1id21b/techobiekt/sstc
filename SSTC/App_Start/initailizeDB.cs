﻿using Microsoft.Ajax.Utilities;
using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static SSTC.Enum.Enums;

namespace SSTC.App_Start
{
    public class InitailizeDB
    {
        /// <summary>
        /// Metoda inicjalizująca pustą baze danych do testów i poprawnego działania programu
        /// </summary>
        public static void Initialize()
        {
            using (SQLiteDBContext db = new SQLiteDBContext())
            {
                if (!db.User.Any())
                {
                    Role admin = new Role() { Name = "Admin" };
                    Role manager = new Role() { Name = "Manager" };
                    Role user = new Role() { Name = "User" };
                    Role accountant = new Role() { Name = "Accountant" };

                    Contract def = new Contract() { Id = 0, DateOfContract = DateTime.Now, DateOfContractEnd = DateTime.Now, GrossAmount = 3.0, HourlyRateProject = 8.0, HoursDaily = 8 };

                    db.Role.Add(admin);
                    db.Role.Add(manager);
                    db.Role.Add(user);
                    db.Role.Add(accountant);

                    db.Contract.Add(def);
                    db.User.Add(new User() { Login = "test123", Password = "test123", Contract = def, Role = admin });
                    Contract _con = new Contract() { Id = 0, DateOfContract = DateTime.Now, DateOfContractEnd = DateTime.Now, GrossAmount = 3.0, HourlyRateProject = 8.0, HoursDaily = 8 };
                    db.User.Add(new User() { Login = "init123", Password = "init123", Contract = _con, Role = manager });

                    db.Registry.Add(new Registry() {NumOfHours = TimeSpan.Parse("08:00:00"), Category = db.Category.Where(c => c.Id == 4).First(), Project = db.Project.Where(p => p.Id == 9).First(), Comment = "EEE no kura działam", NumOfOvertime = TimeSpan.Zero, PayOvertime = false, RegDate= DateTime.Now,Status = (int)RegistryStatus.W_Akceptacji, User = db.User.Where(u => u.Id == 1).First() });
                    db.SaveChanges();
                }


            }
        }

    }
}