﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class PayOvertimeDTO
    {
        public bool Value { get; set; }
        public String Display { get; set; }
    }
}