﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class ProjectMembersDTO
    {
        public List<MembersDTO> Members { get; set; }
        public long ProjectId { get; set; }

    }
}