﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class CalendarDTO
    {
        public int DaysOfMonth { get; set; } //ilość dni w miesiącu
        public int FirstDayOfMonth { get; set; } //dzień tygodnia od którego zaczyna się miesiąc
        public int Month { get; set; }
        public int Year { get; set; }
        public int Day { get; set; }
        public bool Start { get; set; }

        public int FreeDayStart { set; get; }
        public int FreeDayFinish { set; get; }

        public List<int> FreeDays { set; get; }

        public DateTime Today { get; set; } //dzisiejszy dzień
        public List<RegistryDTO> Registries { get; set; } //wpisy

        public CalendarDTO()
        {
            Day = 0;
            Start = false;
            Today = DateTime.Now;
        }
    }
}