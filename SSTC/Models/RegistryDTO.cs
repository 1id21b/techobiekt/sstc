﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class RegistryDTO
    {
        public List<Registry> Registries { get; set; }
        public bool IsFullDay { get; set; }
        public bool IsReject { get; set; }
        public bool IsAccept1 { get; set; }
        public bool IsAccept2 { get; set; }


        public List<Category> Categories { get; set; }
        public List<Project> Projects { get; set; }
        public List<Project> DisplayProjects { get; set; }

        public Registry Registry { get; set; } 

        public List<PayOvertimeDTO> PayList { get; set; }

        public DateTime dt { get; set; }
        public long currentCategory { get; set; }

        public RegistryDTO()
        {
            PayList = new List<PayOvertimeDTO>();
            DisplayProjects = new List<Project>();
            PayOvertimeDTO pay = new PayOvertimeDTO();
            PayOvertimeDTO pay2 = new PayOvertimeDTO();
            pay.Value = false;
            pay.Display = "Rozlicz w urlopie";
            PayList.Add(pay);
            pay2.Value = true;
            pay2.Display = "Wypłać";
            PayList.Add(pay2);
        }
    }
}