﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class MonthlyDTO
    {
        public User user { get; set; }
        public List<Registry> registries { get; set; }

        public int numOfWorkedDays { get; set; }
        public int numOfWorkedHours { get; set; }
        public int numOfOvertime { get; set; }
        public double FullSalary { get; set; }

    }
}