﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class UsersRegistryDTO
    {
        public int current { get; set; }
        public List<User> users { get; set; }
        public List<Registry> reg { get; set; }
    }
}