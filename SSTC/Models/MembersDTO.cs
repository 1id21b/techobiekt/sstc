﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class MembersDTO
    {
        public User User { get; set; }
        public bool InProject { get; set; }

        public MembersDTO()
        {
            InProject = false;
        }
    }
}