﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSTC.Models
{
    public class ProjectDTO
    {
        public Project project { get; set; }
        public List<User> userList { get; set; }
        public int managerId { get; set; }
        public int categoryId { get; set; }

        public ProjectDTO()
        {
        }

        public ProjectDTO(Project project, List<User> userList)
        {
            this.project = project;
            this.userList = userList;
        }
    }
}