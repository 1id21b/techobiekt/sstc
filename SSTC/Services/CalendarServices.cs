﻿using Microsoft.EntityFrameworkCore;
using SSTC.Entity;
using SSTC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static SSTC.Enum.Enums;

namespace SSTC.Services
{
    public class CalendarServices
    {
        SQLiteDBContext db = new SQLiteDBContext();
        public List<RegistryDTO> FillRegistries(int year, int month, int deyOfMonth, User user)
        {
            List<RegistryDTO> registriesDTO = new List<RegistryDTO>();
            registriesDTO = new List<RegistryDTO>();         
            for (int i = 1; i < deyOfMonth + 1; i++)
            {
                RegistryDTO registryDTO = new RegistryDTO();
                TimeSpan sumHours = new TimeSpan(0, 0, 0);
                registryDTO.IsAccept1 = true;
                registryDTO.IsAccept2 = true;
                try
                {                  
                    registryDTO.Registries = db.Registry.Where(r => r.RegDate.Year == year && r.RegDate.Month == month && r.RegDate.Day == i && r.User.Id == user.Id).ToList();
                    foreach (var el in registryDTO.Registries)
                    {
                        sumHours = sumHours + el.NumOfHours;
                        if(el.Status == 4)
                        {
                            registryDTO.IsReject = true;
                        }
                        if(el.Status != 2)
                        {
                            registryDTO.IsAccept1 = false;
                        }
                        if(el.Status != 3)
                        {
                            registryDTO.IsAccept2 = false;
                        }
                    }
                    if (sumHours.Equals(new TimeSpan(user.Contract.HoursDaily, 0, 0)))
                    {
                        registryDTO.IsFullDay = true;
                    }
                    else if(sumHours.Equals(new TimeSpan(0, 0, 0)))
                    {
                        registryDTO.IsFullDay = true;
                    }
                    else
                    {
                        registryDTO.IsFullDay = false;
                    }
                }
                catch (Exception)
                {
                    registryDTO.IsFullDay = true;
                }
                registriesDTO.Add(registryDTO);
            }
            return registriesDTO;
        }
        public CalendarDTO SetMotions(CalendarDTO calendar, User user)
        {
            List<Motion> motions = db.Motion.Include(u => u.User).Where(m => ((m.StartDate.Year == calendar.Year && m.StartDate.Month == calendar.Month) || (m.EndDate.Year == calendar.Year && m.EndDate.Month == calendar.Month)) && m.Status != 4 && m.User.Id == user.Id).ToList();
            calendar.FreeDays = new List<int>();
            foreach (Motion el in motions)
            {
                if(el.StartDate.Month == calendar.Month)
                {
                    calendar.FreeDayStart = el.StartDate.Day;
                }
                else
                {
                    calendar.FreeDayStart = 1;
                }
                if(el.EndDate.Month == calendar.Month)
                {
                    calendar.FreeDayFinish = el.EndDate.Day;
                }
                else
                {
                    calendar.FreeDayFinish = calendar.DaysOfMonth;
                }
                int j = calendar.FreeDays.Count;

                for (int i = calendar.FreeDayStart;i < calendar.FreeDayFinish + 1;i++)
                {
                    calendar.FreeDays.Add(i);
                }
            }
        return calendar;
        }

        public int CalculateFreeDays(User _u)
        {
            double normalDays = 20;
            Contract _c = new Contract();
             _c = db.Contract.Where(c => c.Id == _u.Contract.Id).First();
            List<Motion> motions = db.Motion.Where(m => m.User.Id == _u.Id && m.Status != (int)MotionStatus.Odrzucony).ToList();
            int numOfDaysUsed = motions.Sum(m => m.NumOfDays);
            if (_c.DateOfContract.Year == DateTime.Now.Year)
            {
                int quartal = (_c.DateOfContract.Month / 4) + 1;
                normalDays -= (quartal * 5);
                normalDays = (double)normalDays * ((double)_c.HoursDaily / 8.0 );
                return (int)normalDays - numOfDaysUsed;
            }
            normalDays = (double)normalDays * ((double)_c.HoursDaily / 8.0);
            return (int)normalDays - numOfDaysUsed;
        }
    }
}