﻿using Microsoft.EntityFrameworkCore;
using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace SSTC.Security
{
    /// <summary>
    /// Przeciązenie klasy RoleProvider w celu by sprawdzać klasy które sami utworzyliśmy.
    /// </summary>
    public class SstcRoleProvider : RoleProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Metoda zbierająca z bazy danych role które dany użytkownik posiada i je zwraca.
        /// </summary>
        /// <param name="username">Nazwa użytkownika</param>
        /// <returns>Role użytkownika</returns>
        public override string[] GetRolesForUser(string username)
        {
            using (SQLiteDBContext _db = new SQLiteDBContext())
            {
                var userRoles = _db.User.Where(u => u.Login == username).Include(u => u.Role).Select(r => r.Role.Name).ToArray();
                return userRoles;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}