﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SSTC.Common
{
    public class SstcRepository
    {
        public static SstcRepositoryParameters GetParameters()
        {
            return new SstcRepositoryParameters();
        }

    }

    /// <summary>
    /// Repozytorium przygotowane w celu łatwiejszego zbierania informacji z kluczy w plikach .config
    /// </summary>
    public class SstcRepositoryContainer
    {
        protected NameValueCollection _container;
        public string this[string name] => _container[name];
        public string this[int index] => _container[index];

        public SstcRepositoryContainer()
        {
            _container = ConfigurationManager.AppSettings;
        }
    }

    /// <summary>
    /// Klasa określająca parametry z których można wyciągnąc wartości
    /// </summary>
    public class SstcRepositoryParameters : SstcRepositoryContainer
    {
        public string dbPath => _container["dbPath"];

        public SstcRepositoryParameters() : base()
        {

        }
    }

}