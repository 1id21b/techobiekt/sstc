﻿using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa;
using System.Deployment.Internal;
using SSTC.Models;
using Microsoft.EntityFrameworkCore;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace SSTC.Controllers
{
    [Authorize(Roles = "Admin,Accountant")]
    public class AccountantController : Controller
    {
        SQLiteDBContext db = new SQLiteDBContext();
        // GET: Accountant
        public ActionResult Raports()
        {

            List<User> users = db.User.Where(u => u.IsDeleted == false).ToList();

            return View(users);
        }
        public ActionResult Monthly(int id)
        {

            User user = db.User.Where(u => u.Id == id).First();

            return new ActionAsPdf("PrintMonthly", new { _id = id, month = DateTime.Now.Month }) { FileName = "Raport-Miesieczny-Za-Miesiac-" + DateTime.Now.ToString("MMMM") + "-" + user.FullName + "-" + DateTime.Now.ToShortDateString() + ".pdf" };
        }
        public ActionResult PrevMonthly(int id)
        {

            User user = db.User.Where(u => u.Id == id).First();

            return new ActionAsPdf("PrintMonthly", new { _id = id, month = DateTime.Now.Month - 1 }) { FileName = "Raport-Miesieczny-Za-Miesiac-" + DateTime.Now.AddMonths(-1).ToString("MMMM") + "-" + user.FullName + "-" + DateTime.Now.ToShortDateString() + ".pdf" };
        }

        public ActionResult PrintMonthly(int _id, int month)
        {

            MonthlyDTO monthly = new MonthlyDTO();
            monthly.user = db.User.Include(u => u.Contract).Where(u => u.Id == _id).First();
            monthly.registries = db.Registry.Include(r=>r.Project).Where(r => r.User.Id == _id && r.RegDate.Month == month).ToList();
            foreach (var r in monthly.registries)
            {
                monthly.numOfWorkedHours += r.NumOfHours.Hours;
                monthly.numOfOvertime += r.NumOfOvertime.Hours;
            }
            monthly.numOfWorkedDays = monthly.numOfWorkedHours / monthly.user.Contract.HoursDaily;
            monthly.FullSalary = monthly.user.Contract.GrossAmount + monthly.user.Contract.HourlyRateProject * monthly.numOfOvertime;

            return View(monthly);
        }
    }
}