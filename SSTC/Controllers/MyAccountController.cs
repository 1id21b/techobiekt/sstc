﻿using Microsoft.EntityFrameworkCore;
using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SSTC.Controllers
{
    [Authorize]
    public class MyAccountController : Controller
    {
        // GET: MyAccount
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Metoda, która pobiera dane zalogowanego użytkownika
        /// </summary>
        /// <returns>Zwraca widok z danymi zalogowanego użytkownika</returns>
        [Authorize]
        public ActionResult MyAccount()
        {
            if (User.Identity.Name != null)
            {
                string login;
                login = User.Identity.Name;
                User user;
                using (SQLiteDBContext dBContext = new SQLiteDBContext())
                {
                    user = dBContext.User.Include(c => c.Contract).Where(a => a.Login.Equals(login)).FirstOrDefault();
                }
                return View(user);
            }
            else
            {
                return View();
            }
        }
        /// <summary>
        /// Metoda zapisująca zmiany zalogowanego użytkownika do bazy
        /// </summary>
        /// <param name="user">Dane uzytkownika z widoku</param>
        /// <returns>Zwraca widok ze zaktualizowanymi danymi użytkownika</returns>
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MyAccount(User user)
        {
            User user2 = new User();

            using (SQLiteDBContext db = new SQLiteDBContext())
            {
                //string login = Session["UserName"].ToString();
                FormsAuthentication.GetAuthCookie(user.Name, false);
                user2 = db.User.Where(a => a.Login.Equals(user.Login)).FirstOrDefault();

                user2.Name = user.Name;
                user2.Surname = user.Surname;
                user2.Mail = user.Mail;
                db.SaveChanges();
            }


            return View(user2);
        }

    }
}