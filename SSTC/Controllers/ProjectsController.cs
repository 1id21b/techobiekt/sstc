﻿using Microsoft.EntityFrameworkCore;
using SSTC.Entity;
using SSTC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSTC.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class ProjectsController : Controller
    {
        SQLiteDBContext db = new SQLiteDBContext();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(int id)
        {
            List<User> users = db.User.Where(u => u.IsDeleted != true).ToList();
            ProjectDTO dto = new ProjectDTO(new Project(), users);
            dto.categoryId = id;
            dto.project.Overtime = true;
            return View(dto);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult Add(ProjectDTO dto)
        {
            dto.project.Manager = db.User.Where(u => u.Id == dto.managerId).First();
            Category cat = db.Category.Where(c => c.Id == (long)dto.categoryId).First();
            dto.project.Category = cat;
            db.Project.Add(dto.project);
            db.SaveChanges();
            return RedirectToAction("EditProjects", "Admin");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Projects(int Id)
        {
            List<Project> _p = db.Project.Include(p => p.Projects).Where(p => p.Category.Id == Id).ToList();
            List<User> _u = db.User.Include(p => p.Projects).ToList();
            return View(_p);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_p"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveProjects(Project _p)
        {
            Project project = db.Project.Include(p => p.Category).Where(p => p.Id == _p.Id).First();
            project.Name = _p.Name;
            project.Overtime = _p.Overtime;
            project.Price = _p.Price;
            project.PayedHours = _p.PayedHours;
            project.ProjectStart = _p.ProjectStart;
            project.ProjectEnd = _p.ProjectEnd;
            db.SaveChanges();
            int Id = (int)project.Category.Id;
            return RedirectToAction("Projects/" + Id);
        }
        /// <summary>
        /// Metoda wyświetlająca pracowników projektu
        /// </summary>
        /// <param name="id">Id projektu</param>
        /// <returns>Widok pracowników dodanych do projektu</returns>
        public ActionResult EditMembers(long id)
        {
            List<User> members = db.User.Where(u => u.IsDeleted == false).ToList();
            List<UserProjects> uProjects = new List<UserProjects>();
            ProjectMembersDTO pMem = new ProjectMembersDTO();
            pMem.ProjectId = id;
            pMem.Members = new List<MembersDTO>();
            Project proj = db.Project.Include(p => p.Manager).Where(pr => pr.Id == id).First();
            long managerId = proj.Manager.Id;
            bool kierownik = false;
            try
            {
                uProjects = db.UserProjects.Where(u => u.Project.Id == id).ToList();

                foreach (User u in members)
                {
                    kierownik = false;
                    MembersDTO membersDTO = new MembersDTO();
                    foreach (var project in uProjects)
                    {
                        if (project.User.Id == u.Id)
                        {
                            membersDTO.InProject = true;
                        }
                    }
                    if (u.Id != managerId)
                    {
                        membersDTO.User = u;
                        pMem.Members.Add(membersDTO);
                    }
                }
            }
            catch (Exception e)
            {
                foreach (User u in members)
                {
                    MembersDTO membersDTO = new MembersDTO();
                    if (u.Id != managerId)
                    {
                        membersDTO.User = u;
                        pMem.Members.Add(membersDTO);
                    }
                }
            }
            return View(pMem);
        }
        public ActionResult BackToProjects(long Id)
        {
            Project project = db.Project.Include(p => p.Category).Where(pr => pr.Id == Id).First();
            return RedirectToAction("Projects/" + project.Category.Id);
        }
        [HttpPost]
        public ActionResult EditMembers(ProjectMembersDTO _p)
        {
            var cos = Request.Form["el.InProject"].Split(',').ToArray();
            var cos2 = Request.Form["User.userId"].Split(',').ToArray();

            Project pr = db.Project.Include(p => p.Projects).Where(p => p.Id == _p.ProjectId).First();
            bool isInProject = false;
            try
            {
                int c = 0;
                for (int i = 0; i < cos2.Length; i++)
                {
                    User user = db.User.Include(u => u.Projects).Where(u => u.Id == int.Parse(cos2[i])).First();
                    List<Project> notNecessary = db.Project.Include(p => p.Projects).ToList();
                    foreach (var up in user.Projects)
                    {
                        if (up.Project.Id == _p.ProjectId)
                        {
                            if (cos[c] == "false")
                            {
                                db.UserProjects.Remove(up);
                            }
                            else
                            {

                                isInProject = true;
                                c++;
                            }
                        }
                    }

                    if (!isInProject && cos[c] == "true")
                    {
                        UserProjects ups = new UserProjects(user, pr);
                        user.Projects.Add(ups);
                        pr.Projects.Add(ups);
                        c++;
                    }
                    db.SaveChanges();
                    isInProject = false;
                    c++;
                }
            }
            catch (InvalidOperationException e)
            {
                for (int i = 0; i < cos2.Length; i++)
                {
                    if (cos[i] == "true")
                    {
                        UserProjects userProject = new UserProjects(db.User.Where(u => u.Id == int.Parse(cos2[i])).First(), db.Project.Where(p => p.Id == _p.ProjectId).First());
                        db.UserProjects.Add(userProject);
                    }
                }
                db.SaveChanges();
            }

            return RedirectToAction("EditMembers/" + _p.ProjectId);
        }
    }
}