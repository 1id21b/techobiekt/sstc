﻿using Microsoft.EntityFrameworkCore;
using SSTC.Entity;
using SSTC.Models;
using SSTC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSTC.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        SQLiteDBContext db = new SQLiteDBContext();
        public ActionResult Calendar()
        {
            CalendarDTO calendar = new CalendarDTO();
            calendar.DaysOfMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            calendar.Month = DateTime.Now.Month;
            calendar.Year = DateTime.Now.Year;
            DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            calendar.FirstDayOfMonth = (int)date.DayOfWeek;

            string userId = User.Identity.Name;
            TimeSpan sumHours = new TimeSpan(0, 0, 0);
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            RegistryDTO registryDTO = new RegistryDTO();
            CalendarServices calendarServices = new CalendarServices();
            calendar.Registries = calendarServices.FillRegistries(calendar.Year, calendar.Month, calendar.DaysOfMonth, user);
            calendar = calendarServices.SetMotions(calendar, user);

            return View(calendar);
        }
        public ActionResult UserCalendar(int month, int year, bool next)
        {
            if (next)
            {
                month++;
                if (month > 12)
                {
                    month = 1;
                    year++;
                }
            }
            else
            {
                month--;
                if (month == 0)
                {
                    month = 12;
                    year--;
                }
            }
            CalendarDTO calendar = new CalendarDTO();
            calendar.DaysOfMonth = DateTime.DaysInMonth(year, month);
            DateTime date = new DateTime(year, month, 1);
            calendar.FirstDayOfMonth = (int)date.DayOfWeek;
            if (calendar.FirstDayOfMonth == 0)
                calendar.FirstDayOfMonth = 7;
            calendar.Month = month;
            calendar.Year = year;

            string userId = User.Identity.Name;
            TimeSpan sumHours = new TimeSpan(0, 0, 0);
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            RegistryDTO registryDTO = new RegistryDTO();
            CalendarServices calendarServices = new CalendarServices();
            calendar.Registries = calendarServices.FillRegistries(calendar.Year, calendar.Month, calendar.DaysOfMonth, user);
            calendar = calendarServices.SetMotions(calendar, user);

            return View("Calendar", calendar);
        }

        public ActionResult UserCalendarClick(int month, int year, int day)
        {
            CalendarDTO calendar = new CalendarDTO();
            calendar.DaysOfMonth = DateTime.DaysInMonth(year, month);
            DateTime date = new DateTime(year, month, 1);
            calendar.FirstDayOfMonth = (int)date.DayOfWeek;
            if (calendar.FirstDayOfMonth == 0)
                calendar.FirstDayOfMonth = 7;
            calendar.Month = month;
            calendar.Year = year;
            calendar.Today = new DateTime(year, month, day);

            string userId = User.Identity.Name;
            TimeSpan sumHours = new TimeSpan(0, 0, 0);
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            RegistryDTO registryDTO = new RegistryDTO();
            CalendarServices calendarServices = new CalendarServices();
            calendar.Registries = calendarServices.FillRegistries(calendar.Year, calendar.Month, calendar.DaysOfMonth, user);
            calendar = calendarServices.SetMotions(calendar, user);

            return View("Calendar", calendar);
        }

        [HttpGet]
        public PartialViewResult RegistryDay(int month, int year, int day)
        {
            string userId = User.Identity.Name;
            TimeSpan sumHours = new TimeSpan(0, 0, 0);
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            RegistryDTO registryDTO = new RegistryDTO();

            //registryDTO.dt = DateTime.Parse(day.ToString()+ "-" + month.ToString()+ "-" + year.ToString());
            registryDTO.currentCategory = db.Category.FirstOrDefault().Id;
            registryDTO.Categories = db.Category.ToList();
            List<Project> projekty = db.Project.Include(p => p.Category).ToList();
            registryDTO.Projects = projekty;
            try
            {
                registryDTO.Registries = db.Registry.Where(r => r.RegDate.Year == year && r.RegDate.Month == month && r.RegDate.Day == day && r.User.Id == user.Id).ToList();
                foreach (var el in registryDTO.Registries)
                {
                    sumHours = +el.NumOfHours;
                }
                if (sumHours.Equals(new TimeSpan(user.Contract.HoursDaily, 0, 0)))
                {
                    registryDTO.IsFullDay = false;
                }
                else
                {
                    registryDTO.IsFullDay = true;
                }
            }
            catch (Exception)
            {
                registryDTO.IsFullDay = false;
            }
            registryDTO.Registry = new Registry();
            registryDTO.Registry.RegDate = new DateTime(year, month, day);
            registryDTO.DisplayProjects = db.Project.Include(p => p.Category).Where(c => c.Category.Id == registryDTO.Categories[0].Id && c.Projects.Any(x => x.User.Equals(user))).ToList();
            registryDTO.Registry.Category = new Category();
            registryDTO.Registry.Category.Id = registryDTO.Categories[0].Id;
            //registryDTO.Registries = db.Registry.Where(r => r.User.Id == user.Id).ToList();

            return PartialView("_DayRegistries", registryDTO);
        }
        public PartialViewResult SwitchProjects(long currId)
        {
            List<Project> proj = db.Project.Include(p => p.Category).Include(p => p.Manager).Include(p => p.Projects).Where(p => p.Category.Id == currId).ToList();

            return PartialView("_DayRegistries", proj);
        }

        public PartialViewResult _projects(long currId)
        {

            string userId = User.Identity.Name;
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            List<Project> proj = db.Project.Include(p => p.Category).Include(p => p.Manager).Include(p => p.Projects).Where(p => p.Category.Id == currId && p.Projects.Any(x => x.User.Equals(user))).ToList();

            return PartialView("_projects", proj);
        }

        [HttpPost]
        public ActionResult SendRegistry(Registry registry)
        {
            string userId = User.Identity.Name;
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            registry.User = user;
            registry.Category = db.Category.Find(registry.Category.Id);
            registry.Project = db.Project.Find(registry.Project.Id);
            db.Registry.Add(registry);
            db.SaveChanges();

            return RedirectToAction("UserCalendarClick", new { year = registry.RegDate.Year, month = registry.RegDate.Month, day = registry.RegDate.Day });
        }

        public ActionResult Motions()
        {
            string userId = User.Identity.Name;
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            CalendarServices cs = new CalendarServices();
            int numOfDays = cs.CalculateFreeDays(user);
            TempData["NumOfDays"] = numOfDays.ToString();
            List<Motion> userMotions = db.Motion.Where(m => m.User.Id == user.Id).ToList();
            return View(userMotions);
        }

        [HttpPost]
        public ActionResult Motions(Motion _m)
        {
            string userId = User.Identity.Name;
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            _m.User = user;
            CalendarServices cs = new CalendarServices();
            int numOfDays = cs.CalculateFreeDays(user);
            if (numOfDays < _m.NumOfDays)
            {
                return RedirectToAction("BadMotions", "User", new { s = "Nie posiadasz tyle dni" });
            }
            db.Motion.Add(_m);
            db.SaveChanges();

            return RedirectToAction("Motions", "User");
        }

        public ActionResult BadMotions(string s)
        {
            string userId = User.Identity.Name;
            User user = db.User.Include(c => c.Contract).Where(u => u.Login.Equals(userId)).First();
            TempData["NumOfDays"] = s;
            List<Motion> userMotions = db.Motion.Where(m => m.User.Id == user.Id).ToList();
            return View("Motions", userMotions);
        }
    }
}