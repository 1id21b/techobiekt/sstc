﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using SSTC.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SSTC.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Metoda zwracająca widok strony głównej
        /// </summary>
        /// <returns>Zwraca widok głwnej strony</returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Metoda zwracająca widok strony O nas
        /// </summary>
        /// <returns>Zwraca widok strony O nas</returns>
        public ActionResult About()
        {
            return View();
        }
        /// <summary>
        /// Metoda zwracająca widok strony Kontakt
        /// </summary>
        /// <returns>Zwraca widok strony Kontakt</returns>
        public ActionResult Contact()
        {
            return View();
        }
        /// <summary>
        /// Metoda zwracająca widok strony logowania
        /// </summary>
        /// <returns>Zwraca widok strony logowania</returns>
        public ActionResult Login()
        {
            return View();
        }


        /// <summary>
        /// Metoda controllera wylogowywująca użytkownika z sesji.
        /// </summary>
        /// <returns>Przekierowanie do widoku logowania</returns>
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            TempData["Message"] = "Wylogowano";

            return RedirectToAction("Login","Home");
        }

        /// <summary>
        /// Metoda która dostaje dane użytkownika widoku następnie sprawdza czy dane te są poprawne i loguje użytkowanika
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Przekierowanie do Mojego konta lub z powrotem do logowania z wiadomością</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (SQLiteDBContext dBContext = new SQLiteDBContext())
                    {
                        User principal = dBContext.User.Include(r => r.Role).Where(a => a.Login.Equals(user.Login) && a.Password.Equals(user.Password)).FirstOrDefault();  //TODO throw password through hash for now works without.

                        if (principal != null && !principal.IsDeleted)
                        {
                            FormsAuthentication.SetAuthCookie(principal.Login, false);

                            TempData["Message"] = "Zalogowano";
                            return RedirectToAction("MyAccount", "MyAccount", principal);
                        }
                        else
                        {
                            TempData["Message"] = "Nieprawidłowy login lub hasło";
                        }
                    }
                }
                catch (Exception e)
                {
                    return View();
                }
            }
            return View(user);
        }
    }
}