﻿using Microsoft.EntityFrameworkCore;
using SSTC.Entity;
using SSTC.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSTC.Controllers
{

    /// <summary>
    /// Controller do podstron administratorskich.
    /// </summary>
    
    public class AdminController : Controller
    {

        private SQLiteDBContext db = new SQLiteDBContext();

        /// <summary>
        /// Prosta metoda przenosząca do widoku Admin/Dashboard
        /// </summary>
        /// <returns>Widok</returns>
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Dashboard()
        {
            return View();
        }

        /// <summary>
        /// Metoda kontrolera przenosząca do widoku w którym widzoczna jest lista użytkowników.
        /// </summary>
        [Authorize(Roles = "Admin")]
        public ActionResult EditUsers()
        {

            List<User> users = db.User.Include(u => u.Contract).Where(u => u.IsDeleted == false).ToList();

            return View(users);
        }

        /// <summary>
        /// Metoda typu POST obsługująca formularz zawarty w widoku. Zapisuje edytowane informacje o użytkowniku.
        /// </summary>
        /// <param name="user">Uzytkownik przekazany z widoku do controllera</param>
        /// <returns>Przekierowanie do controllera EditUsers by odświerzyć widok</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult EditUsers(User user)
        {
            string login = Request.Form["item.Login"].ToString();
            User u = db.User.Include(e => e.Contract).Where(i => i.Login == login).First();
            u.Name = Request.Form["item.Name"].ToString();
            u.Surname = Request.Form["item.Surname"].ToString();
            u.Mail = Request.Form["item.Mail"].ToString();
            u.Contract.DateOfContract = DateTime.Parse(Request.Form["item.Contract.DateOfContract"]);
            u.Contract.DateOfContractEnd = DateTime.Parse(Request.Form["item.Contract.DateOfContractEnd"]);
            u.Contract.HourlyRateProject = Double.Parse(Request.Form["item.Contract.HourlyRateProject"]);
            u.Contract.GrossAmount = Double.Parse(Request.Form["item.Contract.GrossAmount"]);
            u.Contract.HoursDaily = int.Parse(Request.Form["item.Contract.HoursDaily"]);

            db.SaveChanges();
            return RedirectToAction("EditUsers", "Admin");
        }

        /// <summary>
        /// Tworzy obiekt nowego użytkownika i przekazuje go do widoku Create.
        /// </summary>
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            User us = new User();
            return View(us);
        }

        /// <summary>
        /// Metoda typu POST która zapisuje nowo utworzonego użytkowanika do bazy danych.
        /// </summary>
        /// <param name="_u"> Obiekt użytkownika z wypełnionymi danymi z formularza w widoku </param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(User _u)
        {
            Role _r = db.Role.Where(r => r.Id.Equals(1)).First();
            _u.Role = _r;
            _u.Password = "Init123";
            _u.Projects = new List<UserProjects>();
            db.User.Add(_u);
            db.SaveChanges();
            return RedirectToAction("EditUsers", "Admin");
        }

        /// <summary>
        /// Prosta metoda usuwająca rekord użytkowanika z bazy danych
        /// TODO - dodać dodatkowe pole do bazy i zmieniac ten parametr zamiast usuwania osob z bazy.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteUser(int id)
        {
            User _u = db.User.Where(u => u.Id == id).First();
            _u.IsDeleted = true;
            db.SaveChanges();
            return RedirectToAction("EditUsers", "Admin");
        }

        // ----------------- KATEGORIE I PROJEKTY -----------------

        /// <summary>
        /// Metoda zwracająca widok tworzenia nowej kategorii
        /// </summary>
        /// <returns>Widok dodawania kategorii</returns>
        [Authorize(Roles = "Admin")]
        public ActionResult CreateCategory()
        {
            Category cat = new Category();
            return View(cat);
        }
        /// <summary>
        /// Metoda wyszukująca wszystkie kategorie wraz z ich projektami
        /// </summary>
        /// <returns>Widok wyświetlający wszystkie kategorie i projekty</returns>
        [Authorize(Roles = "Admin")]
        public ActionResult EditProjects()
        {
            List<Category> categories = db.Category.ToList();
            //List<Category> categories = db.Category.Join(db.Project, c => c.Id, p => p.Id, (c, p) => new { Projects = p }).ToList();
            foreach (Category c in categories)
            {
                c.Projects = db.Project.Where(p => p.Category.Id == c.Id).ToList();
            }
            Category cat = new Category();
            var tuple = new Tuple<List<Category>, Category>(categories, cat);

            return View(tuple);
        }
        /// <summary>
        /// Dodawanie kategorii
        /// </summary>
        /// <param name="category"></param>
        /// <returns>Widok wyświetlający kategorie i projekty</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult AddCategory([Bind(Prefix = "Item2")]Category category)
        {
            if (category != null && !category.Name.Equals(""))
            {
                db.Category.Add(category);
                db.SaveChanges();
            }

            return RedirectToAction("EditProjects", "Admin");
        }
        /// <summary>
        /// Usuwanie kategorii
        /// </summary>
        /// <param name="id">Identyfikator kategorii do usunięcia</param>
        /// <returns>Widok kategorii i projektów</returns>
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteCategory(int id)
        {
            Category category = db.Category.Where(c => c.Id == id).First();
            List<Project> projects = db.Project.Where(p => p.Category.Id == category.Id).ToList();
            category.Projects = projects;
            if (projects.Count > 0)
            {
                //TODO: message that you cant delete it before deteing all projects in it, or just mark it as is deleted so it wont show up.
            }


            db.Category.Remove(category);
            db.SaveChanges();

            return RedirectToAction("EditProjects", "Admin");
        }
        /// <summary>
        /// Metoda edytująca dane kategorii
        /// </summary>
        /// <param name="c">Zmodyfikowane dane kategorii</param>
        /// <returns>Widok kategorii i projektów po aktualizacji</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult SaveEditCategory([Bind(Prefix = "Item2")]Category c)
        {
            String name = Request.Form["item.Name"].ToString();
            long id = long.Parse(Request.Form["item.Id"].ToString());
            Category category = db.Category.Where(i => i.Id == id).First();
            category.Name = name;
            db.SaveChanges();

            return RedirectToAction("EditProjects", "Admin");
        }
    }
}