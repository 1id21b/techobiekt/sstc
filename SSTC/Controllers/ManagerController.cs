﻿using Microsoft.EntityFrameworkCore;
using SSTC.Entity;
using SSTC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static SSTC.Enum.Enums;

namespace SSTC.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    public class ManagerController : Controller
    {
        private SQLiteDBContext db = new SQLiteDBContext();
        public ActionResult Registry()
        {
            UsersRegistryDTO ur = new UsersRegistryDTO();
            string login = User.Identity.Name;
            User logedUser = db.User.Include(u => u.Projects).Include(u => u.Role).Where(u => u.Login.Equals(login)).First();
            if (logedUser.Role.Id == (int)UserRoles.Manager)
            {
                var up = db.UserProjects.Include(a => a.User).ToList();
                var projects = db.Project.Include(p => p.Projects).Include(p => p.Manager).ToList();
                List<Project> proj = new List<Project>();
                List<User> users = new List<User>();
                foreach (var p in projects)
                {
                    if (p.Manager.Equals(logedUser))
                    {
                        proj.Add(p);
                        foreach (var u in p.Projects)
                            if (!users.Contains(u.User))
                            {
                                users.Add(u.User);
                            }
                    }
                }
                ur.users = users;
                ur.current = (int)ur.users.FirstOrDefault().Id;
                ur.reg = new List<Registry>();
            }
            else
            {
                ur.users = db.User.Where(u => u.IsDeleted == false).ToList();
                ur.current = (int)ur.users.FirstOrDefault().Id;
                ur.reg = db.Registry.Include(r => r.Category).Include(r => r.Project).Where(r => r.User.Id == ur.current && (r.Status != (int)RegistryStatus.Odrzucony && r.Status != (int)RegistryStatus.Zatwierdzony_Etap_2)).ToList();
            }



            return View(ur);
        }
        [HttpPost]
        public ActionResult Registry(UsersRegistryDTO ur)
        {
            string login = User.Identity.Name;
            User logedUser = db.User.Include(u => u.Projects).Include(u => u.Role).Where(u => u.Login.Equals(login)).First();
            if (logedUser.Role.Id == (int)UserRoles.Manager)
            {
                var up = db.UserProjects.Include(a => a.User).ToList();
                var projects = db.Project.Include(p => p.Projects).Include(p => p.Manager).ToList();
                List<Project> proj = new List<Project>();
                List<User> users = new List<User>();
                foreach (var p in projects)
                {
                    if (p.Manager.Equals(logedUser))
                    {
                        proj.Add(p);
                        foreach (var u in p.Projects)
                            if (!users.Contains(u.User))
                            {
                                users.Add(u.User);
                            }
                    }
                }
                ur.users = users;
                ur.reg = db.Registry.Include(r => r.Category).Include(r => r.Project).Where(r => r.User.Id == ur.current && r.Status == (int)RegistryStatus.W_Akceptacji && proj.Contains(r.Project)).ToList();
            }
            else
            {
                ur.users = db.User.Where(u => u.IsDeleted == false).ToList();
                ur.reg = db.Registry.Include(r => r.Category).Include(r => r.Project).Where(r => r.User.Id == ur.current && (r.Status != (int)RegistryStatus.Odrzucony && r.Status != (int)RegistryStatus.Zatwierdzony_Etap_2)).ToList();
            }

            return View(ur);
        }

        public ActionResult Accept(int _id)
        {
            Registry reg = db.Registry.Where(r => r.Id == _id).First();
            string login = User.Identity.Name;
            User logedUser = db.User.Include(u => u.Projects).Include(u => u.Role).Where(u => u.Login.Equals(login)).First();
            if (logedUser.Role.Id == (int)UserRoles.Admin)
            {
                reg.Status = (int)RegistryStatus.Zatwierdzony_Etap_2;
            }
            else
            {
                reg.Status = (int)RegistryStatus.Zatwierdzony_Etap_1;
            }
            db.SaveChanges();
            return RedirectToAction("Registry");
        }
        public ActionResult Reject(int _id)
        {
            Registry reg = db.Registry.Where(r => r.Id == _id).First();
            reg.Status = (int)RegistryStatus.Odrzucony;
            db.SaveChanges();
            return RedirectToAction("Registry");
        }

        /// <summary>
        /// TODO
        /// </summary>
        /// <returns></returns>
        public ActionResult Motions()
        {
            string login = User.Identity.Name;
            User loggedUser = db.User.Include(u => u.Projects).Include(u => u.Role).Where(u => u.Login.Equals(login)).First();

            List<Motion> motions = new List<Motion>();
            if (loggedUser.Role.Id == (int)UserRoles.Admin)
            {
                motions = db.Motion.Include(m => m.User).Where(m => m.Status == (int)RegistryStatus.W_Akceptacji || m.Status == (int)RegistryStatus.Zatwierdzony_Etap_1).ToList();
            }
            else
            {
                var projects = db.Project.Include(p => p.Projects).Include(p => p.Manager).ToList();
                db.User.Load();
                List<User> users = new List<User>();
                foreach (var p in projects)
                {
                    if (p.Manager.Equals(loggedUser))
                    {
                        foreach (var u in p.Projects)
                            if (!users.Contains(u.User))
                            {
                                users.Add(u.User);
                            }
                    }
                }

                motions = db.Motion.Include(m => m.User).Where(m => users.Contains(m.User) && m.Status == (int)RegistryStatus.W_Akceptacji).ToList();

            }


            return View(motions);
        }

        public ActionResult AcceptMotion(int _id)
        {
            Motion mot = db.Motion.Where(r => r.Id == _id).First();
            string login = User.Identity.Name;
            User logedUser = db.User.Include(u => u.Projects).Include(u => u.Role).Where(u => u.Login.Equals(login)).First();
            if (logedUser.Role.Id == (int)UserRoles.Admin)
            {
                mot.Status = (int)MotionStatus.Zatwierdzony;
            }
            else
            {
                mot.Status = (int)MotionStatus.Zatwierdzony_Etap_1;
            }
            db.SaveChanges();
            return RedirectToAction("Motions");
        }
        public ActionResult RejectMotion(int _id)
        {
            Motion mot = db.Motion.Where(r => r.Id == _id).First();
            mot.Status = (int)MotionStatus.Odrzucony;
            db.SaveChanges();
            return RedirectToAction("Motions");
        }
    }
}