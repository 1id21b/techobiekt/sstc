﻿using Microsoft.EntityFrameworkCore;
using SSTC.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class SQLiteDBContext : DbContext
    {
        public DbSet<Role> Role { get; set; }
        public DbSet<Contract> Contract { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<Registry> Registry { get; set; }
        public DbSet<Motion> Motion { get; set; }
        public DbSet<UserProjects> UserProjects { get; set; }

        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var _dbPath = SstcRepository.GetParameters().dbPath;
            string path = _dbPath + @"\Database.sqlite";
            optionsBuilder.UseSqlite("Data Source=" + path);

            //string path = Environment.CurrentDirectory + @"\Database.sqlite";
            //optionsBuilder.UseSqlite("Data Source=" + path);
        }
    }

}