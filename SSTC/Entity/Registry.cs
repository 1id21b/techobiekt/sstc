﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class Registry
    {
        [Key]
        public long Id { set; get; }
        public TimeSpan NumOfHours { set; get; } //liczba zarejestrowanych godzin we wpisie
        public TimeSpan NumOfOvertime { set; get; } //liczba rarejestrowanych nadgodzin we wpisie
        public Boolean PayOvertime { set; get; } //true-wypłacaj nadgodziny false-licz do urlopu
        public string Comment { set; get; } //Opis 
        public int Status { set; get; } //Status wpisu {Odrzucony/Zaakceptowany/Niezweryfikowany}
        public DateTime RegDate { set; get; } //Data zarejestrowania wpisu

        public Category Category { set; get; }
        public Project Project { set; get; }
        public User User { set; get; }
    }
}