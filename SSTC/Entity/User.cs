﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class User
    {
        [Key]
        public long Id { set; get; }
        public string Name { set; get; }
        public string Surname { set; get; }
        public string Login { set; get; }
        public string Password { set; get; }
        public string Mail { set; get; }
        [DefaultValue("false")]
        public bool IsDeleted { set; get; }

        public Contract Contract { set; get; }
        public Role Role { set; get; }
        public ICollection<UserProjects> Projects { set; get; }

        [NotMapped]
        public string FullName {
             get { return Name + " " + Surname; }
             set { FullName = value; }
        }

    }
}