﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class UserProjects
    {
        [Key]
        public long Id { get; set; }
        public User User { get; set; }
        public Project Project { get; set; }

        public UserProjects()
        {
        }

        public UserProjects(User user, Project project)
        {
            User = user;
            Project = project;
        }
    }
}