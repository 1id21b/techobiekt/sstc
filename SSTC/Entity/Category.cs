﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class Category
    {
        [Key]
        public long Id{ set; get; }
        public string Name { set; get; }
        [NotMapped]
        public ICollection<Project> Projects { set; get; }
    }
}