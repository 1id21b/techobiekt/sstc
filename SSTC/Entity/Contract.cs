﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class Contract
    {
        [Key]
        public long Id { set; get; }
        public DateTime DateOfContract { set; get; } //Data podpisania/obowiązywania kontraktu
        public DateTime DateOfContractEnd { set; get; } //Data do końca kontraktu (data końca świata czas nieokreślony)
        public double HourlyRateProject { set; get; } //Projektowa stawka za godzine
        public double GrossAmount { set; get; } //Kwota brutto umowy
        public int HoursDaily { set; get; } //Godzin dziennie do zarejestrowania
    }
}