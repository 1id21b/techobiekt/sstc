﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class Role
    {
        [Key]
        public long Id { set; get; }
        public string Name { set; get; }
    }
}