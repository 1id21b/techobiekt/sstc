﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class Project
    {
        [Key]
        public long Id { set; get; }
        public string Name { set; get; }
        public double Price { set; get; } //Cena projektu
        public TimeSpan PayedHours { set; get; } //Liczba godzin dla projektu do zarejestrowania
        public DateTime ProjectStart { set; get; } //Od kiedy można rejestrować
        public DateTime ProjectEnd { set; get; } //Do kiedy mozna rejestrować
        public Boolean Overtime { set; get; } //Zezwolenie na nadgodziny

        public Category Category { set; get; }
        public User Manager { set; get; } //Kierownik projektu weryfikujący wpisy
        public ICollection<UserProjects> Projects { set; get; } //Uczestnicy projektu

        public Project()
        {
        }

        public Project(Category category)
        {
            Category = category;
        }
    }
}