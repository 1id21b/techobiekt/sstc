﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSTC.Entity
{
    public class Motion
    {
        [Key]
        public long Id { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public int Status { set; get; }
        public int NumOfDays { set; get; } //Wyliczona liczba dni urlopowych

        public User User { set; get; }
    }
}